#!/bin/bash

#Based on https://github.com/ThomasLeister/root-certificate-deployment

### Script installs all certs .crt on CERTS folder to certificate trust store of applications using NSS
### (e.g. Firefox, Thunderbird, Chromium)
### Mozilla uses cert8, Chromium and Chrome use cert9

###
### Requirement: apt install libnss3-tools
###

CERTUTIL=$(which certutil)
[[ $CERTUTIL = '' ]]  && echo "certutil not found. Please install libnss3-tools" && exit

###
### CA folder to install
###
CERTS="/usr/local/share/ca-certificates/"


for certfile in $(find $CERTS -name *.crt)
do
  echo "Installing certificate $certfile"

  ##
  ### For cert8 (legacy - DBM)
  ##
  if [ ! -d ~/.mozilla/firefox ]
  then
    timeout -k 1 3s firefox --headless > /dev/null 2>&1
  fi

  for certDB in $(find ~/.mozilla/ -writable -name "cert8.db")
  do
    certdir=$(dirname ${certDB});
    certname=$(basename ${certfile});
    certutil -A -n ${certname} -t "C,," -i ${certfile} -d dbm:${certdir}
  done


  ###
  ### For cert9 (SQL)
  ###
  if [ ! -d "~/.pki" ]
  then
    timeout -k 3 5s google-chrome --headless > /dev/null 2>&1
  fi  
  
  for certDB in $(find ~/.pki/ -writable -name "cert9.db")
  do
    certdir=$(dirname ${certDB});
    certname=$(basename ${certfile});
    certutil -A -n ${certname} -t "C,," -i ${certfile} -d sql:${certdir}
  done  

done
