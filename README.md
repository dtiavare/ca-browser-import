# README

The script **ca-browser-import.sh** installs all certificates (.crt) present on **/usr/local/share/ca-certificates/** folder to certificate trust store of applications using NSS
(e.g. Firefox, Thunderbird, Chromium)
Mozilla uses cert8, Chromium and Chrome use cert9

# Important: 
This script applies the certificates per-user (not system-wide), so if you want install certificates for all users, you should put the script at startup for all of them.

The easiest way to do this, is create a symbolic link to script on **/etc/profile.d/**

`ln -s /etc/profile.d/ca-browser-import.sh /path/to/ca-browser-import/ca-browser-import.sh`